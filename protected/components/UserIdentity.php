<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{

		Yii::import('application.models.Userdetail');
		$users = new Userdetail();

		$username = $this->username;
		$password = $this->password;

		$users=Userdetail::model()->find(array('condition'=>"UDEmail = '$username' "));
		if(!empty($users)){

			if($password == $users->UDPhoneNumber)
			{
				
				$session = Yii::app()->session;
                $session['username']= $users->UDEmail;
                $session['name']= $users->UDFirstname. ' ' .$users->UDLastname;
                $session['id']= $users->UDKey;
                $session['avatar']= $users->UDUrl_profile;
				$this->errorCode=self::ERROR_NONE;

			}
			else{
				//var_dump($password); exit();
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
		}
		else{
			//die("you are logged in out");
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}

		return !$this->errorCode;
	}
}