<?php

/**
 * This is the model class for table "userdetail".
 *
 * The followings are the available columns in table 'userdetail':
 * @property string $UDKey
 * @property integer $UDSso_id
 * @property string $UDSsoUnique
 * @property string $UDFirstname
 * @property string $UDLastname
 * @property string $UDPhoneNumber
 * @property string $UDEmail
 * @property string $UDAddress
 * @property string $UDDateOfBirth
 * @property string $UDGender
 * @property string $UDUrl_profile
 * @property string $UDStatus
 * @property string $UDCreated
 * @property string $created
 */
class Userdetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userdetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UDSso_id, UDLastname, UDPhoneNumber, UDDateOfBirth, UDGender, UDStatus, UDCreated', 'required'),
			array('UDSso_id', 'numerical', 'integerOnly'=>true),
			array('UDSsoUnique, UDGender', 'length', 'max'=>45),
			array('UDFirstname, UDDateOfBirth', 'length', 'max'=>50),
			array('UDLastname, UDEmail, UDCreated', 'length', 'max'=>100),
			array('UDPhoneNumber', 'length', 'max'=>25),
			array('UDAddress', 'length', 'max'=>255),
			array('UDUrl_profile', 'length', 'max'=>250),
			array('UDStatus', 'length', 'max'=>10),
			array('created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UDKey, UDSso_id, UDSsoUnique, UDFirstname, UDLastname, UDPhoneNumber, UDEmail, UDAddress, UDDateOfBirth, UDGender, UDUrl_profile, UDStatus, UDCreated, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'UDKey' => 'Udkey',
			'UDSso_id' => 'Udsso',
			'UDSsoUnique' => 'Udsso Unique',
			'UDFirstname' => 'Udfirstname',
			'UDLastname' => 'Udlastname',
			'UDPhoneNumber' => 'Udphone Number',
			'UDEmail' => 'Udemail',
			'UDAddress' => 'Udaddress',
			'UDDateOfBirth' => 'Uddate Of Birth',
			'UDGender' => 'Udgender',
			'UDUrl_profile' => 'Udurl Profile',
			'UDStatus' => 'Udstatus',
			'UDCreated' => 'Udcreated',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('UDKey',$this->UDKey,true);
		$criteria->compare('UDSso_id',$this->UDSso_id);
		$criteria->compare('UDSsoUnique',$this->UDSsoUnique,true);
		$criteria->compare('UDFirstname',$this->UDFirstname,true);
		$criteria->compare('UDLastname',$this->UDLastname,true);
		$criteria->compare('UDPhoneNumber',$this->UDPhoneNumber,true);
		$criteria->compare('UDEmail',$this->UDEmail,true);
		$criteria->compare('UDAddress',$this->UDAddress,true);
		$criteria->compare('UDDateOfBirth',$this->UDDateOfBirth,true);
		$criteria->compare('UDGender',$this->UDGender,true);
		$criteria->compare('UDUrl_profile',$this->UDUrl_profile,true);
		$criteria->compare('UDStatus',$this->UDStatus,true);
		$criteria->compare('UDCreated',$this->UDCreated,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Userdetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
