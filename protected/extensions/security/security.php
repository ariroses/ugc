<?php
/*
 * Author       : Indra Octama
 * Purposed To  : Secure The POST data from device to server 
 * Created Date : 1 Oktober 2014
 * Update Date  : 6 Oktober 2014 (sesudah encrypt --> base64encode, sebelum decrypt --> base64decode) 
 * Credit To    : Firman Syamsuddin
 */

class security {
   
    
      public  function wrap($txt){
            $blah = $this->keygen(3);
            $ilen = mt_rand(10, 15);
            $lencode = dechex($ilen);
            $key = $this->keygen($ilen);
            $encdata = $this->fnEncrypt($txt, $key);
            $wrapped = $blah.$lencode.$key.$encdata;
            return base64_encode($wrapped);
        }

      public  function baca($txt)
        {
          $txt = base64_decode($txt);
            $len = hexdec(substr($txt,3,1));
            $truekey = substr($txt,4,$len);
            $data = substr($txt,4+$len);
            $decrypted = $this->fnDecrypt($data, $truekey);
            return $decrypted;
        }

      private  function keygen($length=10)
        {
                $key = '';
                list($usec, $sec) = explode(' ', microtime());
                mt_srand((float) $sec + ((float) $usec * 100000));

                $inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));

                for($i=0; $i<$length; $i++)
                {
                    $key .= $inputs{mt_rand(0,61)};
                }
                return $key;
        }

       private function fnEncrypt($sValue, $sSecretKey)
        {
            $sSecretKey = hash('MD5', $sSecretKey, true);
            $padding = 16 - (strlen($sValue) % 16);
            $paddingchar = chr($padding);
            $sValue.= str_repeat($paddingchar, $padding);
            return rtrim(
                base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $sSecretKey, $sValue, MCRYPT_MODE_ECB,
                        str_repeat("\0", 16)
                        )
                    ), "\0"
                );
        }



       private function fnDecrypt($sValue, $sSecretKey)
        {
            $sSecretKey = hash('MD5', $sSecretKey, true);
            $sValue = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $sSecretKey, base64_decode($sValue), MCRYPT_MODE_ECB,
                    str_repeat("\0", 16)), "\0");
            $padding = ord($sValue[strlen($sValue) - 1]);
            return substr($sValue,0,-$padding);
        }


    
}


