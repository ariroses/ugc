<?php
/*
 * Author : Indra Octama
 * Created Date : 23 Maret 2015
 * Purpose : Extension SimpleCurl
 * 
 */

class SimpleCurl{
    
    private $_url;
    
    public function __construct() {
        
        $this->_url = Yii::app()->params["API_URL"];

    }
    
    /*
     * How to use :
     * example
     * @location = "product/GetProduct"
     * @data_json = {blabla:blabla}
     */
    public function __exec($location,$data_string = ""){
        
        $ch = curl_init($this->_url."/".$location);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                 
        curl_setopt($ch, CURLOPT_POSTFIELDS,
           "teks=".$data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                                           
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
        
    }

  
}