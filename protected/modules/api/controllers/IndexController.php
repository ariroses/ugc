<?php

class IndexController extends Controller
{

	private $_mont;
	private $_udkey;
	private $_date_90_day;
	private $_date_70_day;
	private $_date_28_day;
	private $_date_2_day;
	private $_date_1_day;
	private $_analytic;
	private $_gettoken;
	private $_viewid;

	public function init(){
        
        parent::init();

        Yii::import('application.modules.api.components.google-api.vendor.*');
		require_once('autoload.php');

        $this->_analytic = $this->initializeAnalytics();
        $this->_gettoken = $this->initializeAnalyticstoken();
        $this->_viewid = "187245160";
        
        $dDaysc1 = new DateTime('today - 90 days');
        $dDaysc2 = new DateTime('today - 70 days');
        $dDaysc3 = new DateTime('today - 28 days');
        $dDaysc4 = new DateTime('today - 2 days');
        $dDaysc5 = new DateTime('today - 1 days');

		$this->_date_90_day = $dDaysc1->format('Y-m-d');
		$this->_date_70_day = $dDaysc2->format('Y-m-d');
		$this->_date_28_day = $dDaysc3->format('Y-m-d');
		$this->_date_2_day = $dDaysc4->format('Y-m-d');
		$this->_date_1_day = $dDaysc5->format('Y-m-d');

        $this->_udkey = Yii::app()->session['id'];
        $this->_mont = array ("","Jan", "Feb", "Mar", 
                     "Apr","Mei","Jun",
	                 "Jul","Agt","Sep", 
			  	     "Okt", "Nov", "Des");
    }

	public function actionIndex()
	{
		 echo "API";
	}

	protected function initializeAnalytics()
	{

	  // Use the developers console and download your service account
	  // credentials in JSON format. Place them in this directory or
	  // change the key file location if necessary.
	  $KEY_FILE_LOCATION = Yii::app()->basePath . '/modules/api/components/google-api/service-account-credentials.json';

	  // Create and configure a new client object.
	  $client = new Google_Client();
	  $client->setApplicationName("Hello Analytics Reporting");
	  $client->setAuthConfig($KEY_FILE_LOCATION);
	  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	  
	  $analytics = new Google_Service_AnalyticsReporting($client);

	  return $analytics;
	}

	protected function initializeAnalyticstoken()
	{

	  // Use the developers console and download your service account
	  // credentials in JSON format. Place them in this directory or
	  // change the key file location if necessary.
	  $KEY_FILE_LOCATION = Yii::app()->basePath . '/modules/api/components/google-api/service-account-credentials.json';

	  // Create and configure a new client object.
	  $client = new Google_Client();
	  $client->setApplicationName("Hello Analytics Reporting");
	  $client->setAuthConfig($KEY_FILE_LOCATION);
	  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	  
	  $client->refreshTokenWithAssertion();
	  $token = $client->getAccessToken();
	  $accessToken = $token['access_token'];

	  return $accessToken;

	}

	public function actionGetdasbor(){

		header('Content-Type: application/json');

		$between_subscriber = $this->querySubscriberdate();
		$count_subscriber = $this->querySubscribercount();
		$count28_subscriber = $this->querySubscribercount28days();
		$subscriber = $this->querySubscriber();
		$last_podcast = $this->queryGetlastpodcast();
		$podcast_last_time = $this->queryGetpodcastslasttime($this->_date_2_day);

		if(count($last_podcast)>0){

			if(count($podcast_last_time) > 0){
				$podcast_last_time_ = $this->queryGetpodcastslasttime($this->_date_2_day);
			}else{
				$podcast_last_time_ = $this->queryGetpodcastslasttime($this->_date_90_day);
			}

			print_r(json_encode(
				[
					'date_subscriber'=>@$between_subscriber,
					'count_subscriber'=>@$count_subscriber[0]['count'],
					'count28_subscriber'=>@$count28_subscriber[0]['count'],
					'subscriber'=>@$subscriber,
					'last_podcast'=>@$last_podcast,
					'podcast_last_time'=>@$podcast_last_time_[0]['title'],
					'podcast_last_time_view'=>'',
					'date_ago'=>@$this->__time_elapsed_string(@$last_podcast[0]['create_date'],true),
					'podcasts_count'=>@$this->queryCountplay(@$last_podcast[0]['id']),
					'count_like'=>@$this->queryCountlikes(@$last_podcast[0]['id']),
					'count_comment'=>@$this->queryCountcomment(@$last_podcast[0]['id']),
					'percent'=>$this->getPercentpodcasts(@$podcast_last_time_[0]['title']),
				])
			);
		}else{
			print_r(json_encode(
				[
					'date_subscriber'=>'',
					'count_subscriber'=>'',
					'count28_subscriber'=>'',
					'subscriber'=>'',
					'last_podcast'=>'',
					'podcast_last_time'=>'',
					'podcast_last_time_view'=>'',
					'date_ago'=>'',
					'podcasts_count'=>'',
					'count_like'=>'',
					'count_comment'=>'',
					'percent'=>''
				])
			);
		}


	}

	public function actionGetpodcasts(){

		header('Content-Type: application/json');
		
		$data_podcasts = $this->queryGetpodcasts();

		if(count($data_podcasts) > 0){
			$dt = [];
			foreach ($data_podcasts as $key => $value) {
				$dh = [];
				$dh['id'] = $value['id'];
				$dh['title'] = $value['title'];
				$dh['image_name'] = $value['image_name'];
				$dh['status'] = $value['status'];
				$dh['moderation'] = $value['moderation_status'];
				$dh['create_date'] = $value['create_date'];
				$dh['count_play'] = $this->queryCountplay($value['id']);
				$dh['count_likes'] = $this->queryCountlikes($value['id']);
				$dh['count_coment'] = $this->queryCountcomment($value['id']);

				array_push($dt, $dh);
			}
		}else{
			$dt = [];
		}

		print_r(json_encode(
			[
				'podcasts'=>$dt,
			])
		);
	}

	public function actionGetcontentpodcasts($id){
		header('Content-Type: application/json');
		$content_podcasts = $this->queryGetcontentpodcasts($id);
		print_r(json_encode(
			[
				'content_podcasts'=>$content_podcasts,
			])
		);
	}

	public function actionGetkomen(){
		
		header('Content-Type: application/json');

		$podcasts_user = $this->queryGetpodcasts();
		$arr_id = [];
		$data = [];

		if(count($podcasts_user)> 0){
			foreach ($podcasts_user as $key => $val) {
				array_push($arr_id, $val['id']);
			}
		
			$comment = $this->queryAllcommentsuser(implode(',',$arr_id));

			foreach ($comment as $key => $v) {
				$dt = [];
				$data_reply = $this->queryCommentreply($v['id']);
				$dt['id_coment'] = $v['id'];
				$dt['profile'] = $v['UDUrl_profile'];
				$dt['name'] = $v['UDFirstname'].' '.$v['UDLastname'];
				$dt['message'] = $v['message'];
				$dt['podcast_title'] = $v['title'];
				$dt['podcast_image'] = $v['image_name'];
				$dt['podcast_description'] = $v['description'];
				$dt['reply'] = $data_reply;
				$dt['count_reply'] = count($data_reply);
				$dt['count_likes'] = $this->queryPodcastcommentslikescount($v['id'])[0]['count_like'];
				array_push($data,$dt);
			}
		}

		echo json_encode($data);
	}

	protected function querySubscriberdate(){
		$sql = "SELECT count(created_at) as y, DATE(created_at) as x 
				FROM subscribe WHERE DATE(created_at) BETWEEN '2019-02-15' and DATE(now())
				AND subscribe_id = '".$this->_udkey."' 
				group by DATE(created_at)";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$date_between_subscriber = $command->queryAll();

		return $date_between_subscriber;

	}

	protected function querySubscribercount(){
		$sql = "SELECT count(subscribe_id) as count FROM subscribe WHERE subscribe_id = '".$this->_udkey."'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count_subscriber = $command->queryAll();

		return $count_subscriber;

	}

	protected function querySubscribercount28days(){
		$sql = "SELECT count(subscribe_id) as count FROM subscribe 
				WHERE subscribe_id = '".$this->_udkey."' 
				AND DATE(created_at) between '".$this->_date_28_day."' and DATE(now())";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count28_subscriber = $command->queryAll();

		return $count28_subscriber;

	}

	protected function querySubscriber(){
		$sql = "SELECT user.UDFirstname,user.UDLastname,user.UDUrl_profile,sub.created_at FROM subscribe AS sub
				LEFT JOIN userdetail AS user ON sub.user_id = user.UDKey 
				WHERE sub.subscribe_id = '".$this->_udkey."'
				AND DATE(created_at) between '".$this->_date_90_day."' and DATE(now())";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count_subscriber = $command->queryAll();

		return $count_subscriber;

	}

	protected function queryGetlastpodcast(){
		$sql = "SELECT pod.id,pod.title,pod.create_date,pod.image_name as image FROM podcasts AS pod
				LEFT JOIN userdetail AS user ON pod.sso_id = user.UDSso_id
				WHERE user.UDKey = '".$this->_udkey."'
				AND pod.categoryPodcasts_id !='6'
				AND pod.categoryPodcasts_id !='14'
				AND pod.moderation_status = '0'
				ORDER BY pod.id DESC LIMIT 1";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$last_podcast = $command->queryAll();

		return $last_podcast;

	}

	protected function queryGetpodcasts(){
		$sql = "SELECT pod.id,pod.title,pod.create_date,pod.image_name,pod.status,pod.moderation_status,
				sum(c_con.total_counter) as count_play,count(DISTINCT comen.id) as count_coment, count(DISTINCT lik.id) as count_likes
				FROM podcasts as pod
				LEFT JOIN comments as comen ON pod.id = comen.target_id AND comen.target_type = 'podcasts'
				LEFT JOIN likes as lik ON pod.id = lik.content_id AND lik.type = 'Podcast'
				LEFT JOIN userdetail AS user ON pod.sso_id = user.UDSso_id
				LEFT JOIN content_podcasts as c_pod ON pod.id = c_pod.podcasts_id
				LEFT JOIN contents_counter as c_con ON c_pod.id = c_con.content_id AND c_con.type = 'Podcast'
				WHERE user.UDKey = '".$this->_udkey."'
				AND pod.categoryPodcasts_id !='6'
				AND pod.categoryPodcasts_id !='14'
				AND pod.moderation_status = '0'
				GROUP BY pod.id
				ORDER BY pod.id DESC";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$podcasts = $command->queryAll();

		return $podcasts;
	}

	protected function queryGetpodcastslasttime($date_last_time){
		$sql = "SELECT pod.title,pod.create_date FROM podcasts as pod
				LEFT JOIN userdetail as user ON pod.sso_id = user.UDSso_id
				LEFT JOIN content_podcasts as c_pod ON pod.id = c_pod.podcasts_id
				LEFT JOIN contents_counter as c_con ON c_pod.id = c_con.content_id AND c_con.type = 'Podcast'
				WHERE user.UDKey = '".$this->_udkey."'
				AND pod.categoryPodcasts_id !='6'
				AND pod.categoryPodcasts_id !='14'
				AND pod.moderation_status = '0'
				AND DATE(c_con.modified_date) between '".$date_last_time."' and DATE(now())
				GROUP BY pod.id
				ORDER BY c_con.modified_date DESC LIMIT 2";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$podcasts = $command->queryAll();

		return $podcasts;
	}

	protected function queryGetpodcastcount($id_podcast){
		$sql = "SELECT pod.id,pod.title,c_con.total_counter FROM podcasts as pod
				LEFT JOIN content_podcasts as c_pod ON pod.id = c_pod.podcasts_id
				LEFT JOIN contents_counter as c_con ON c_pod.id = c_con.content_id AND c_con.type = 'Podcast'
				WHERE pod.id = '".$id_podcast."'
				AND pod.categoryPodcasts_id !='6'
				AND pod.categoryPodcasts_id !='14'
				AND pod.moderation_status = '0'
				GROUP BY pod.id LIMIT 1";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$podcasts_count = $command->queryAll();

		return $podcasts_count;
	}

	protected function queryGetcountcomment($id_podcast){
		$sql = "SELECT count(comen.id) as count_comment FROM podcasts as pod
				LEFT JOIN comments as comen ON pod.id = comen.target_id AND comen.target_type = 'podcasts'
				WHERE pod.id = '".$id_podcast."'
				AND pod.categoryPodcasts_id !='6'
				AND pod.categoryPodcasts_id !='14'
				AND pod.moderation_status = '0'
				GROUP BY pod.id";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$comment_count = $command->queryAll();

		return $comment_count;
	}

	protected function queryGetcountlike($id_podcast){
		$sql = "SELECT count(lik.id) as count_like FROM podcasts as pod
				LEFT JOIN likes as lik ON pod.id = lik.content_id AND lik.type = 'Podcast'
				WHERE pod.id = '".$id_podcast."'
				AND pod.categoryPodcasts_id !='6'
				AND pod.categoryPodcasts_id !='14'
				AND pod.moderation_status = '0'
				GROUP BY pod.id";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$comment_count = $command->queryAll();

		return $comment_count;
	}

	protected function queryAllcommentsuser($array_id_podcasts){
		$sql = "SELECT coment.id,conpod.id as id_content,coment.message,user.UDFirstname,user.UDLastname,conpod.title,
				conpod.image_name,conpod.description,user.UDUrl_profile
				FROM comments as coment
				LEFT JOIN userdetail as user ON coment.user_id = user.UDKey 
				LEFT JOIN podcasts as conpod ON coment.target_id = conpod.id
				WHERE coment.target_type = 'podcasts'
				AND conpod.categoryPodcasts_id !='6'
				AND conpod.categoryPodcasts_id !='14'
				AND conpod.moderation_status = '0'
				AND coment.target_id in(".$array_id_podcasts.")";

		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$comment = $command->queryAll();

		return $comment;
	}

	protected function queryCommentreply($id_comment){
		$sql = "SELECT coment.id,coment.message,user.UDFirstname,user.UDLastname,user.UDUrl_profile
				FROM comments as coment
				LEFT JOIN userdetail as user ON coment.user_id = user.UDKey 
				WHERE coment.target_type = 'comments'
				AND coment.target_id = ".$id_comment."";

		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$comment_reply = $command->queryAll();

		return $comment_reply;
	}

	protected function queryContentpodcast($id_podcast){
		$sql = "SELECT con_pod.title,con_pod.image_name,con_pod.moderation_status,
				con_pod.status,con_pod.created_date,c_con.total_counter 
				FROM content_podcasts as con_pod
				LEFT JOIN contents_counter as c_con ON con_pod.id = c_con.content_id AND c_con.type = 'Podcast'
				WHERE con_pod.podcasts_id = ".$id_podcast."";

		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$content_podcast = $command->queryAll();

		return $content_podcast;
	}

	protected function queryPodcastcommentslikescount($id_comment){
		$sql = "SELECT count(id) as count_like FROM likes
				WHERE type = 'comments'
				AND content_id = ".$id_comment."";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count_like_comments = $command->queryAll();

		return $count_like_comments;
	}

	protected function queryGetcontentpodcasts($id_podcast){
		$sql = "SELECT c_pod.id,c_pod.title,c_pod.status,c_pod.image_name,c_pod.created_date,c_con.total_counter as count_play
				FROM content_podcasts AS c_pod
				LEFT JOIN contents_counter as c_con ON c_pod.id = c_con.content_id AND c_con.type = 'Podcast'
				WHERE c_pod.podcasts_id = ".$id_podcast."
				GROUP BY c_pod.id
				ORDER BY c_pod.id DESC";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$content_podcasts = $command->queryAll();

		return $content_podcasts;	
	}

	protected function queryCountplay($id){
		$sql = "SELECT sum(c_con.total_counter) as count FROM podcasts as pod 
				LEFT JOIN content_podcasts as c_pod ON pod.id = c_pod.podcasts_id
				LEFT JOIN contents_counter as c_con ON c_pod.id = c_con.content_id AND c_con.type = 'Podcast'
				WHERE pod.id = ".$id;
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count = $command->queryAll();

		return $count[0]['count'];	
	}

	protected function queryCountcomment($id){
		$sql = "SELECT count(id) as count FROM comments WHERE target_id = ".$id." AND target_type = 'podcasts'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count = $command->queryAll();

		return $count[0]['count'];	
	}

	protected function queryCountlikes($id){
		$sql = "SELECT count(id) as count FROM likes WHERE content_id = ".$id." AND type = 'Podcast'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$count = $command->queryAll();

		return $count[0]['count'];	
	}

	protected function getDuration($title){
		$data = $this->sumView($this->Result($this->googleApis_get_event_podcast($title)),1);
		$data_ = $this->__convertMinutes($this->__averageSessionduration($data));
		return $data_; 
	}

	public function actionGetkuy(){
		header('Content-Type: application/json');

		$podcast_last_time = $this->queryGetpodcastslasttime($this->_date_2_day);

		$title = $podcast_last_time[0]['title'];

		$play = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_28_day,$this->_date_1_day)),0);
		$play_old = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_70_day,$this->_date_28_day)),0);

		$duration = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_28_day,$this->_date_1_day)),1);
		$duration_old = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_70_day,$this->_date_28_day)),1);


		if($play > $play_old){
			$style_play = 'text-success';
			$icon_play = 'zmdi zmdi-trending-up zmdi-hc-fw';
			if($play == 0 && $play_old == 0){
				$percen_play = 0;
			}else{
				$percen_play = number_format((float)($play_old/$play)*100,2,'.','');
			}
			if($percen_play == 0){
				$percen_play = 100;
			}
		}else{
			$style_play = 'text-danger';
			$icon_play = 'zmdi zmdi-trending-down zmdi-hc-fw';
			if($play == 0 && $play_old == 0){
				$percen_play = 0;
			}else{
				$percen_play = number_format((float)($play/$play_old)*100,2,'.','');
			}
			if($percen_play == 0){
				$percen_play = 100;
			}
		}
		if($duration > $duration_old){
			$style_duration = 'text-success';
			$icon_duration = 'zmdi zmdi-trending-up zmdi-hc-fw';
			if($duration == 0 && $duration_old == 0){
				$percen_duration = 0;
			}else{
				$percen_duration = number_format((float)($duration_old/$duration)*100,2,'.','');
			}
			if($percen_duration == 0){
				$percen_duration = 100;
			}
		}else{
			$style_duration = 'text-danger';
			$icon_duration = 'zmdi zmdi-trending-down zmdi-hc-fw';
			if($duration == 0 && $duration_old == 0){
				$percen_duration = 0;
			}else{
				$percen_duration = number_format((float)($duration/$duration_old)*100,2,'.','');
			}
			if($percen_duration == 0){
				$percen_duration = 100;
			}
		}
		
		echo json_encode([
				'play'=>$style_play,
				'play_icon'=>$icon_play,
				'play_percen'=>$percen_play,
				'duration'=>$style_duration,
				'duration_icon'=>$icon_duration,
				'duration_percen'=>$percen_duration
			]);
	}

	protected function googleApis_get_event_podcast($label_name){
		// Create the DateRange object.
		$dateRange = new Google_Service_AnalyticsReporting_DateRange();
		$dateRange->setStartDate($this->_date_2_day);
		$dateRange->setEndDate("today");

		$eventAction = new Google_Service_AnalyticsReporting_Dimension();
		$eventAction->setName("ga:eventAction");

		$eventCategory = new Google_Service_AnalyticsReporting_Dimension();
		$eventCategory->setName("ga:eventCategory");

		$eventLabel = new Google_Service_AnalyticsReporting_Dimension();
		$eventLabel->setName("ga:eventLabel");

		$date = new Google_Service_AnalyticsReporting_Dimension();
		$date->setName("ga:date");

		// Create the Metrics object.
		$totalEvents = new Google_Service_AnalyticsReporting_Metric();
		$totalEvents->setExpression("ga:totalEvents");
		$totalEvents->setAlias("totalEvents");

		// Create the Metrics object.
		$duration = new Google_Service_AnalyticsReporting_Metric();
		$duration->setExpression("ga:sessionDuration");
		$duration->setAlias("duration");

		$ordering = new Google_Service_AnalyticsReporting_OrderBy();
		$ordering->setFieldName("ga:date");
		$ordering->setOrderType("VALUE");
		$ordering->setSortOrder("DESCENDING");

		// Create the DimensionFilter.
	  	$dimensionFilter_label = new Google_Service_AnalyticsReporting_DimensionFilter();
	  	$dimensionFilter_label->setDimensionName('ga:eventLabel');
	  	$dimensionFilter_label->setOperator('REGEXP');
	  	$dimensionFilter_label->setExpressions([$label_name]);

	  	$dimensionFilter_action = new Google_Service_AnalyticsReporting_DimensionFilter();
	  	$dimensionFilter_action->setDimensionName('ga:eventAction');
	  	$dimensionFilter_action->setOperator('EXACT');
	  	$dimensionFilter_action->setExpressions(['Play']);


	  	// Create the DimensionFilterClauses
	  	$dimensionFilterClause1 = new Google_Service_AnalyticsReporting_DimensionFilterClause();
	  	$dimensionFilterClause1->setFilters(array($dimensionFilter_label));

	  	$dimensionFilterClause2 = new Google_Service_AnalyticsReporting_DimensionFilterClause();
	  	$dimensionFilterClause2->setFilters(array($dimensionFilter_action));

		// Create the ReportRequest object.
		$request = new Google_Service_AnalyticsReporting_ReportRequest();
		$request->setViewId($this->_viewid);
		$request->setDateRanges($dateRange);
		$request->setDimensions(array($eventAction,$eventCategory,$eventLabel,$date));
		$request->setDimensionFilterClauses(array($dimensionFilterClause1,$dimensionFilterClause2));
		$request->setMetrics(array($totalEvents,$duration));
		$request->setOrderBys($ordering);

		$body = new Google_Service_AnalyticsReporting_GetReportsRequest();
		$body->setReportRequests(array($request));

		return $this->_analytic->reports->batchGet($body);
	}


	protected function googleApis_get_event_podcast_ds($label_name,$start,$end){

		// Create the DateRange object.
		$dateRange = new Google_Service_AnalyticsReporting_DateRange();
		$dateRange->setStartDate($start);
		$dateRange->setEndDate($end);

		$eventAction = new Google_Service_AnalyticsReporting_Dimension();
		$eventAction->setName("ga:eventAction");

		$eventCategory = new Google_Service_AnalyticsReporting_Dimension();
		$eventCategory->setName("ga:eventCategory");

		$eventLabel = new Google_Service_AnalyticsReporting_Dimension();
		$eventLabel->setName("ga:eventLabel");

		// Create the Metrics object.
		$totalEvents = new Google_Service_AnalyticsReporting_Metric();
		$totalEvents->setExpression("ga:totalEvents");
		$totalEvents->setAlias("totalEvents");

		// Create the Metrics object.
		$duration = new Google_Service_AnalyticsReporting_Metric();
		$duration->setExpression("ga:sessionDuration");
		$duration->setAlias("duration");

		// Create the DimensionFilter.
	  	$dimensionFilter_label = new Google_Service_AnalyticsReporting_DimensionFilter();
	  	$dimensionFilter_label->setDimensionName('ga:eventLabel');
	  	$dimensionFilter_label->setOperator('REGEXP');
	  	$dimensionFilter_label->setExpressions([$label_name]);

	  	$dimensionFilter_action = new Google_Service_AnalyticsReporting_DimensionFilter();
	  	$dimensionFilter_action->setDimensionName('ga:eventAction');
	  	$dimensionFilter_action->setOperator('EXACT');
	  	$dimensionFilter_action->setExpressions(['Play']);


	  	// Create the DimensionFilterClauses
	  	$dimensionFilterClause1 = new Google_Service_AnalyticsReporting_DimensionFilterClause();
	  	$dimensionFilterClause1->setFilters(array($dimensionFilter_label));

	  	$dimensionFilterClause2 = new Google_Service_AnalyticsReporting_DimensionFilterClause();
	  	$dimensionFilterClause2->setFilters(array($dimensionFilter_action));

		// Create the ReportRequest object.
		$request = new Google_Service_AnalyticsReporting_ReportRequest();
		$request->setViewId($this->_viewid);
		$request->setDateRanges($dateRange);
		$request->setDimensions(array($eventAction,$eventCategory,$eventLabel));
		$request->setDimensionFilterClauses(array($dimensionFilterClause1,$dimensionFilterClause2));
		$request->setMetrics(array($totalEvents,$duration));

		$body = new Google_Service_AnalyticsReporting_GetReportsRequest();
		$body->setReportRequests(array($request));

		return $this->_analytic->reports->batchGet($body);
	}

	protected function getPercentpodcasts($title){
		header('Content-Type: application/json');
		$play = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_28_day,$this->_date_1_day)),0);
		$play_old = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_70_day,$this->_date_28_day)),0);

		$duration = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_28_day,$this->_date_1_day)),1);
		$duration_old = $this->sumView($this->Result($this->googleApis_get_event_podcast_ds($title,$this->_date_70_day,$this->_date_28_day)),1);


		if($play > $play_old){
			$style_play = 'text-success';
			$icon_play = 'zmdi zmdi-trending-up zmdi-hc-fw';
			if($play == 0 && $play_old == 0){
				$percen_play = 0;
			}else{
				$percen_play = number_format((float)($play_old/$play)*100,2,'.','');
			}
			if($percen_play == 0){
				$percen_play = 100;
			}
		}else{
			$style_play = 'text-danger';
			$icon_play = 'zmdi zmdi-trending-down zmdi-hc-fw';
			if($play == 0 && $play_old == 0){
				$percen_play = 0;
			}else{
				$percen_play = number_format((float)($play/$play_old)*100,2,'.','');
			}
			if($percen_play == 0){
				$percen_play = 100;
			}
		}
		if($duration > $duration_old){
			$style_duration = 'text-success';
			$icon_duration = 'zmdi zmdi-trending-up zmdi-hc-fw';
			if($duration == 0 && $duration_old == 0){
				$percen_duration = 0;
			}else{
				$percen_duration = number_format((float)($duration_old/$duration)*100,2,'.','');
			}
			if($percen_duration == 0){
				$percen_duration = 100;
			}
		}else{
			$style_duration = 'text-danger';
			$icon_duration = 'zmdi zmdi-trending-down zmdi-hc-fw';
			if($duration == 0 && $duration_old == 0){
				$percen_duration = 0;
			}else{
				$percen_duration = number_format((float)($duration/$duration_old)*100,2,'.','');
			}
			if($percen_duration == 0){
				$percen_duration = 100;
			}
		}
		
		return [
				'play'=>$style_play,
				'play_icon'=>$icon_play,
				'play_percen'=>$percen_play,
				'duration'=>$style_duration,
				'duration_icon'=>$icon_duration,
				'duration_percen'=>$percen_duration
			];
	}

	protected function sumView($data,$t){

		if(count($data) > 0){
			$dt_d = [];
			foreach ($data as $k => $v) {
				foreach ($v['metrics'] as $key => $val) {

					$dt = [];
					$dt = (integer)$val['values'][$t];
					array_push($dt_d, $dt);
				}
			}

			if(count($dt_d)>0){

				$group = array();
				$sum = 0;

				foreach ($dt_d as $key => $v) {
					$sum += $v;
				}

				if ($sum){
					return $sum;
				}else{
					return 0;
				}

			}else{
				return 0;
			}

		}else{
			return 0;
		}

	}

	protected function Result($reports) {

	  for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
	    $report = $reports[ $reportIndex ];
	    $header = $report->getColumnHeader();
	    $dimensionHeaders = $header->getDimensions();
	    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
	    $rows = $report->getData()->getRows();

	    return $rows;
	  }
	}

    
    private function __compressImage($url,$file,$resolusi){

    	$info=getimagesize($url);
		if ($info['mime']=='image/jpeg') $image=imagecreatefromjpeg($url);
		else if ($info['mime']=='image/gif') $image=imagecreatefromjpeg($url);
		else if ($info['mime']=='image/png') $image=imagecreatefromjpeg($url);
		

		//simpan gambar destination_url
		imagejpeg($image, $url, $resolusi);
		//return destination file url
		return $url;
    
    }

    private function __dateUpdate($tanggal)
	// Mengubah format yyyy-mm-dd (format MYSQL) 
	//          menjadi dd  nama_bulan yyyy
	{
	   $bulan = $this->_mont;
	   
	   $y = (integer) substr($tanggal,0,4);
	   $m = (integer) substr($tanggal,5,2);
	   $d = (integer) substr($tanggal,8,2);

	   $jam = substr($tanggal,11,5);

	   return $d." ".$bulan[$m]." ".$y. " ".$jam ;
	}

	private function __time_elapsed_string($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'tahun',
	        'm' => 'bulan',
	        'w' => 'minggu',
	        'd' => 'hari',
	        'h' => 'jam',
	        'i' => 'menit',
	        's' => 'detik',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full){
	    	$string = array_slice($string, 0, 1);
	    }else{
	    	$string = array_slice($string, 0, 2);
	    }

	    return $string ? implode(', ', $string) . ' pertama' : 'sekarang';
	}

	private function __averageSessionduration($arrayDuration){

		//Calculate the average.
		$average = ceil(array_sum($arrayDuration) / count($arrayDuration));

		//Print out the average.
		return $average;
	}

	private function __convertMinutes($seconds) {
		// CONVERT TO HH:MM:SS
		$hours = floor($seconds/3600);
		$remainder_1 = ($seconds % 3600);
		$minutes = floor($remainder_1 / 60);
		$seconds = ($remainder_1 % 60);
 
		// PREP THE VALUES
		if(strlen($hours) == 1) {
			$hours = "0".$hours;
		}
 
		if(strlen($minutes) == 1) {
			$minutes = "0".$minutes;
		}
 
		if(strlen($seconds) == 1) {
			$seconds = "0".$seconds;
		}
 
		return $hours.":".$minutes.":".$seconds;
     
    }
}