<?php

/**
 * This is the model class for table "contents_counter".
 *
 * The followings are the available columns in table 'contents_counter':
 * @property integer $id
 * @property integer $content_id
 * @property string $type
 * @property integer $total_counter
 * @property string $status
 * @property integer $created
 * @property string $created_date
 * @property integer $modified
 * @property string $modified_date
 */
class ContentsCounter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contents_counter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content_id, total_counter, created, modified', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>50),
			array('status', 'length', 'max'=>1),
			array('created_date, modified_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, content_id, type, total_counter, status, created, created_date, modified, modified_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content_id' => 'Content',
			'type' => 'Type',
			'total_counter' => 'Total Counter',
			'status' => 'Status',
			'created' => 'Created',
			'created_date' => 'Created Date',
			'modified' => 'Modified',
			'modified_date' => 'Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content_id',$this->content_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('total_counter',$this->total_counter);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified',$this->modified);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContentsCounter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
