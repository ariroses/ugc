<style>
	@media screen and (min-width: 768px) {

		.cstm-form{
			margin-left: 30px;	
		}

	}
</style>
<div class="col-sm-10 col-offset-6 cstm-form" >
	<div class="row justify-content-md-center">
		<center>
			<img class="img img-responsive" src="<?= Yii::app()->theme->baseUrl ?>/img/roov copy.png" width="120">
		</center>
	</div>
	<br/>
	<div class="form floating-label">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
			'validateOnSubmit'=>true,
			),
		)); ?>

		<div class="form-group">
			<?php echo $form->textField($model,'username',array('class'=>'form-control','id'=>'username')); ?>
			<label for="username">Email</label>
			<?php echo $form->error($model,'username', array('class'=>'help-block')); ?>

		</div>
		<div class="form-group">
			<?php echo $form->passwordField($model,'password',array('class'=>'form-control','id'=>'password')); ?>
			<label for="password">Phone</label>
			<?php echo $form->error($model,'password', array('class'=>'help-block')); ?>
			<!-- <p class="help-block"><a href="#">Forgotten?</a></p> -->
		</div>
		<br/>
		<div class="row">
			<div class="col-xs-6 text-left">
				<div class="checkbox checkbox-inline checkbox-styled">
					<label>
						<input type="checkbox" name="rememberMe"> <span>Remember me</span>
					</label>
				</div>
			</div><!--end .col -->
			<br>
			<br>
			<div class="col-md-12 text-right">
				<?php echo CHtml::submitButton('Login',array('class'=>'col-md-12 btn btn-primary btn-raised',)); ?>
			</div><!--end .col -->
		</div><!--end .row -->
		
			<!-- <span class="text-xs text-primary">RADIO AGGREGATOR V.0.1</span> -->
		
		<?php $this->endWidget(); ?>
	</div>
</div><!--end .col -->


<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);

?>