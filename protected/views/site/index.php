<style>
	.header-nav-profile .dropdown > a {
	    min-width: 80px !important;
	}
	.show{
		display: block;
	}
	.hide{
		display: none;
	}
</style>

<div class="row" style="margin-top:40px;" id="rowDasbor">
	<div class="col-md-12">
		<div class="section-header">
			<ol class="breadcrumb">
				<li class="active">Dashboard</li>
			</ol>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card" id="card-last-podcast">
			<div class="card-head">
				<header><span class="text-xm">Performa Podcast Terbaru</span></header>
			</div><!--end .card-head -->
			<div class="card-body height-11 scroll" style="padding-top: 10px;" id="perfm">
				
			</div><!--end .card-body -->
		</div><!--end .card -->
	</div><!--end .col -->


	<div class="col-md-4">
		<div class="card" id="card-analisis">
			<div class="card-head">
				<header><span class="text-xm">Analisis</span></header>
			</div><!--end .card-head -->
			<div class="card-body height-11 scroll" style="padding-top: 0px;">
				<h4>Subscriber saat ini </h4>
				<h2 id="now_subscriber"></h3>
				<span id="28_subscriber"></span>
				<hr>
				<h4 class="text-normal">Ringkasan</h4>

				<span class="text-default-light">28 hari terakhir</span><br>
				<span>Pemutaran</span><span class="pull-right text-sm" id="Pemutaran"> </span><br>
				<span>Waktu tonton</span><span class="pull-right text-sm" id="Waktu"> </span>
				<hr>
				<h4 class="text-normal">Podcast teratas</h4>
				<span class="text-default-light">48 jam terakhir · Pemutaran </span><br>
				<span id="last_time_podcast"></span><span class="pull-right text-default text-sm" id="last_time_podcast_view"></span>
			</div><!--end .card-body -->
		</div><!--end .card -->
	</div><!--end .col -->

	<div class="col-md-4">
		<div class="card" id="card-subscriber">
			<div class="card-head">
				<header><span class="text-xm">Subscriber baru</span></header>
			</div><!--end .card-head -->
			<div class="card-body height-11 scroll" style="padding-top: 0px;">
				<span class="text-default-light">90 hari terakhir</span>
				<ul class="list divider-full-bleed" id="new_subscriber">
					
				</ul>
			</div><!--end .card-body -->
		</div><!--end .card -->
	</div><!--end .col -->

</div>

<div class="row" style="margin-top:40px;" id="rowPodcast">
	<div class="col-md-12">
		<div class="section-header">
			<ol class="breadcrumb">
				<li class="active">Podcast</li>
			</ol>
		</div>
	</div>

	<div class="col-md-12">
		<div class="card" id="card-podcasts">
    		<div class="card-body">
    			<div class="row" id="divPodcast">
    				
    			</div>
    		</div>
    	</div>
	</div>

</div>

<div class="row" style="margin-top:40px;" id="rowKomen">
	<div class="col-md-12">
		<div class="section-header">
			<ol class="breadcrumb">
				<li class="active">Komentar</li>
			</ol>
		</div>
	</div>

	<div class="col-md-12">
		<div class="card" id="card-comment">
    		<div class="card-body" id="divComment">

			</div>
		</div>
	</div>

</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Content Podcast #<strong id="name"></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -32px;color: red;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewContent">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>