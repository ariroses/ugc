var baseUrl = window.location.pathname;

var lastToast;

//firebase get activeUser
var config = {
	apiKey: "AIzaSyD6JqX8H38idaj4jVovBo8g91dIIV-9VpU",
	databaseURL: "https://roovdev-1ed30.firebaseio.com",
};

firebase.initializeApp(config);
var count = firebase.database().ref("new-roov").child("count");
var message = firebase.database().ref("message").limitToLast(3);

message.on('value', function(snapshot){
	renderMessage(snapshot.val());
});
	
count.on('value', function(snapshot) {
	if(snapshot.val() != 0){
		$('#count').html('<span>'+snapshot.val()+'</span>');
	}      				
});

function cleaRing(){
	firebase.database().ref("new-roov").child("count").set(0);
	document.getElementById("count").innerHTML = '';

}

function renderMessage(Obj){

	var message_html = '';
	message_html += '<li class="dropdown-header">Messages</li>'
					
	if(Obj != null){
		var keys = Object.keys(Obj);
		for(var i=0;i<keys.length;i++){
			console.log(Obj[keys[i]].title);

			message_html += '<li>\
							<a class="alert alert-callout alert-warning" href="javascript:void(0);">\
								<img class="pull-right img-circle dropdown-avatar" style="width:50px; height:50px;max-width: none;" src="'+Obj[keys[i]].image+'" alt="">\
								<strong>'+Obj[keys[i]].name+'</strong><br/>\
								<small>Create Podcasts: <strong>'+Obj[keys[i]].title+'</strong></small>\
							</a>\
						</li>'

		}
	}else{
		message_html += '<center><small> Messages Not Found </small></center>'
	}

	message_html += '<li><a href="#">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>\
					<li onclick="cleaRing()"><a href="#">Clear all messages <span class="pull-right"><i class="fa fa-trash-o"></i></span></a></li>'

	document.getElementById("messagess").innerHTML = message_html;
}