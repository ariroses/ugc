var baseUrl = assetsPath;

console.log(imageUrl);

window.onload = function(){
	subscriberRender();
}
function subscriberRender(){
	$('#rowDasbor').addClass('show');
	$('#rowKomen').addClass('hide');
	$('#rowPodcast').addClass('hide');
	
	$.ajax ({
		type: "POST",
        url: baseUrl+"/api/index/getdasbor",
        data: {"code":77},

        beforeSend: function () {
        	addLoader('card-subscriber');
        	addLoader('card-analisis');
        	addLoader('card-last-podcast');
        },
	
		success: function (data) {
			movLoader('card-subscriber');
			movLoader('card-analisis');
			movLoader('card-last-podcast');

			$('#Dasbor').addClass('active');
			$('#Komen').removeClass('');
			$('#Podcast').removeClass('');
			
			if(data){

            	htmlsubscriber_new = '';

            	html_perfm = '';

				$.each(data.subscriber, function (index, v) {
						htmlsubscriber_new += '<li class="tile">\
						<div class="tile-content" style="padding-left:0px;">\
							<div class="tile-icon">\
								<img src="'+v.UDUrl_profile+'" alt="" />\
							</div>\
							<div class="text-xm">'+v.UDFirstname+' '+v.UDLastname+'</div>\
						</div>\
						<a class="btn btn-flat ink-reaction">\
							<i class="fa fa-external-link text-primary text-sm" style="font-size: 15px;"></i>\
						</a>\
					</li>';
				});

				var imgSub = data.last_podcast[0].image;
				if(imgSub.substr(0,3) == 'htt'){
					var img = data.last_podcast[0].image;
				}else{
					var img = imageUrl+'/upload/'+data.last_podcast[0].image;
				}

				html_perfm += '<img class="img-responsive" src="'+img+'" />\
									<span class="text-default-light">'+data.date_ago+' :</span><br>\
									<span>Pemutaran</span> <span class="pull-right text-default text-sm">'+data.podcasts_count+'</span><br>\
									<span>Suka <span class="pull-right text-default text-sm"> '+data.count_like+' </span><br>\
									<span>Komentar <span class="pull-right text-default text-sm"> ('+data.count_comment+') </span><br>';


				document.getElementById("new_subscriber").innerHTML = htmlsubscriber_new;
				document.getElementById("now_subscriber").innerHTML = data.count_subscriber;
				document.getElementById("28_subscriber").innerHTML = '+'+data.count28_subscriber+' dalam 28 hari terakhir';
				document.getElementById("perfm").innerHTML = html_perfm;
				document.getElementById("last_time_podcast").innerHTML = data.podcast_last_time;
				document.getElementById("last_time_podcast_view").innerHTML = '';
				document.getElementById("Pemutaran").innerHTML = data.percent.play_percen+"%"+" <li class='"+data.percent.play_icon+"'></li>";
				document.getElementById("Waktu").innerHTML = data.percent.duration_percen+"%"+" <li class='"+data.percent.duration_icon+"'></li>";

				var percetP = document.getElementById("Pemutaran");
   				percetP.classList.add(data.percent.play);

   				var percetW = document.getElementById("Waktu");
   				percetW.classList.add(data.percent.duration);
            }	
		}
	});
}


function addLoader(a){
	var card = $('#'+a);
	var container = $('<div class="card-loader"></div>').appendTo(card);
	container.hide().fadeIn();
	var opts = {
		lines: 17, // The number of lines to draw
		length: 0, // The length of each line
		width: 3, // The line thickness
		radius: 6, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 13, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#000', // #rgb or #rrggbb or array of colors
		speed: 2, // Rounds per second
		trail: 76, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9 // The z-index (defaults to 2000000000)
	};
	//var spinner = new Spinner(opts).spin(container.get(0));
	//card.data('card-spinner', spinner);
}

function movLoader(a){
	var card = $('#'+a);
	// var spinner = card.data('card-spinner');
	var loader = card.find('.card-loader');
	loader.fadeOut(function () {
		// spinner.stop();
		loader.remove();
	});
}

function showReply(a){
	console.log(a);
	var t = $('.oke'+a);
	t.toggle();
    html_caret = '<i class="fa fa-angle-up" style="margin-left:10px;" onclick="showReplyup('+a+')"></i>';
    document.getElementById("caret"+a).innerHTML = html_caret;
}
function showReplyup(a){
	console.log(a);
	var t = $('.oke'+a);
	t.toggle();
    html_caret = '<i class="fa fa-angle-down" style="margin-left:10px;" onclick="showReply('+a+')"></i>';
    document.getElementById("caret"+a).innerHTML = html_caret;
}

function loadPodcast(){
	console.log('podcasts');
	$.ajax ({
		type: "POST",
        url: baseUrl+"/api/index/getpodcasts",
        data: {"code":77},

        beforeSend: function () {
        	addLoader('card-podcasts');
        },
	
		success: function (data) {
			movLoader('card-podcasts');
			
			if(data){
				html_podcasts = '';

            	html_podcasts += '<div class="col-lg-12">\
									<div class="table-responsive">'
            	html_podcasts += '<table class="table table-condensed no-margin">\
									<thead>\
										<tr>\
											<th></th>\
											<th>Judul</th>\
											<th>Visibilitas</th>\
											<th>Tanggal</th>\
											<th>Pemutaran</th>\
											<th>Komentar</th>\
											<th>Suka</th>\
										</tr>\
									</thead>\
									<tbody>'
				$.each(data.podcasts, function (index, v) {
						if(v.status == '1'){
							status = 'Publik';
						}else{
							status = 'Private'
						}

						var imgSub = v.image_name;
						if(imgSub.substr(0,3) == 'htt'){
							var img = v.image_name;
						}else{
							var img = imageUrl+'/upload/'+v.image_name;
						}
						html_podcasts += '<tr>\
												<td><img src="'+img+'" class="img-responsive" width="60" /></td>\
												<td>\
												<span class="text-normal text-xm text-primary-dark left">\
												<a href="#" onclick="viewContentpodcast('+v.id+')" data-toggle="modal" data-target="#exampleModal">'+v.title+'</a>\
												</span>\
												</td>\
												<td><i class="fa fa-eye text-primary-dark"></i> '+status+'</td>\
												<td>'+v.create_date+'</td>\
												<td>'+v.count_play+'</td>\
												<td>'+v.count_coment+'</td>\
												<td>'+v.count_likes+'</td>\
											</tr>'
				});

				html_podcasts += '</tbody>\
								</table>\
								<div>\
								<div>'

				document.getElementById("divPodcast").innerHTML = html_podcasts;

            }
		}
	});
}

function loadKomen(){
	console.log('komentar');
	$.ajax ({
		type: "POST",
        url: baseUrl+"/api/index/getkomen",
        data: {"code":77},

        beforeSend: function () {
        	addLoader('card-comment');
        },
	
		success: function (data) {
			movLoader('card-comment');
			
			if(data){
				html_comment = '';

				$.each(data, function (index, v) {
					

					var imgSub = v.podcast_image;
					if(imgSub.substr(0,3) == 'htt'){
						var img = v.podcast_image;
					}else{
						var img = imageUrl+'/upload/'+v.podcast_image;
					}

					html_comment += '<div class="row" style="border-bottom: 1px solid #bcb7b7;">\
										<div class="col-md-8">\
											<ul class="list divider-full-bleed">\
											<li class="tile">\
											<div class="tile-content" style="padding-left:0px;">\
												<div class="tile-icon">\
													<img src="'+v.profile+'" alt="">\
												</div>\
												<div class="tile-text">\
													<span class="text-default-light text-xs">'+v.name+'</span><br>\
													<span class="text-xm">'+v.message+'</span><br><br>\
													<span class="text-default-light" style="font-size: 15px; margin-right: 15px;"> '+v.count_reply+' balasan \
													<span id="caret'+v.id_coment+'"><i class="fa fa-angle-down" style="margin-left:10px;" onclick="showReply('+v.id_coment+')"></i></span>\
													</span>\
													<i class="fa fa-heart text-default-light" style="font-size: 13px; margin-left: 5px; margin-right: 15px;"> '+v.count_likes+'</i>\
												</div>'
												
							html_comment +='</div>\
											</li>\
											</ul>'

											if(v.reply.length > 0){
													$.each(v.reply, function (index, r) {
									html_comment +='<div class="col-md-12 oke'+v.id_coment+'" id="" style="margin-left: 42px;">\
													<ul class="list divider-full-bleed">\
														<li class="tile">\
														<div class="tile-content" style="padding-left:0px;">\
															<div class="tile-icon">\
																<img src="'+r.UDUrl_profile+'" alt="">\
															</div>\
															<div class="tile-text">\
																<span class="text-default-light text-xs">'+r.UDFirstname+' '+r.UDLastname+'</span><br>\
																<span class="text-xm">'+r.message+'</span><br>\
																<i class="fa fa-heart text-default-light" style="font-size: 13px; margin-left: 0px; margin-top: 10px;"> 2</i>\
															</div>\
															<div class="col-md-12">\
																\
															</div>\
														</div>\
														</li>\
													</ul>\
												</div>'
													})												
												} 
									html_comment +='</div>\
										<div class="col-md-4" style="margin-top: 15px;">\
											<div class="col-md-4">\
												<img src="'+img+'" class="height-2">\
											</div>\
											<div class="col-md-8">\
												<span class="text-xm">'+v.podcast_title+'</span><br>\
												<span class="text-xs">'+v.podcast_description.substr(0, 50)+'...</span>\
											</div>\
										</div>\
									</div>'
				});

				document.getElementById("divComment").innerHTML = html_comment;

            }
		}
	});
}

Podcast.addEventListener("click", function(event){ 
   
	$('#Podcast').addClass('active');
	$('#Komen').removeClass('active');
	$('#Dasbor').removeClass('active');

	$('#rowDasbor').removeClass('show');
	$('#rowDasbor').addClass('hide');
	
	$('#rowKomen').removeClass('show');
	$('#rowKomen').addClass('hide');

	$('#rowPodcast').addClass('show');
	$('#rowPodcast').removeClass('hide');

	loadPodcast();
    
},false);

Komen.addEventListener("click", function(event){ 
   
	$('#Komen').addClass('active');
	$('#Podcast').removeClass('active');
	$('#Dasbor').removeClass('active');

	$('#rowDasbor').removeClass('show');
	$('#rowDasbor').addClass('hide');
	
	$('#rowPodcast').removeClass('show');
	$('#rowPodcast').addClass('hide');

	$('#rowKomen').addClass('show');
	$('#rowKomen').removeClass('hide');

	loadKomen();
    
},false);

Dasbor.addEventListener("click", function(event){ 
   
	$('#Dasbor').addClass('active');
	$('#Podcast').removeClass('active');
	$('#Komen').removeClass('active');

	$('#rowPodcast').removeClass('show');
	$('#rowPodcast').addClass('hide');
	
	$('#rowKomen').removeClass('show');
	$('#rowKomen').addClass('hide');

	$('#rowDasbor').addClass('show');
	$('#rowDasbor').removeClass('hide');

	subscriberRender();
    
},false);


function viewContentpodcast(id){

        $.ajax({
            type: "POST",
            url: baseUrl+"/api/index/getcontentpodcasts/id/"+id,
            dataType: 'json',
            data: { id: id },
            beforeSend: function () {
                console.log("viewContentpodcast");
            },
            success: function (data) {
            	if(data){
				html_c_podcasts = '';

            	html_c_podcasts += '<div class="table-responsive">'
            	html_c_podcasts += '<table class="table table-condensed no-margin">\
									<thead>\
										<tr>\
											<th></th>\
											<th>Judul</th>\
											<th>Visibilitas</th>\
											<th>Tanggal</th>\
											<th>Pemutaran</th>\
										</tr>\
									</thead>\
									<tbody>'
				$.each(data.content_podcasts, function (index, v) {
						if(v.status == '1'){
							status = 'Publik';
						}else{
							status = 'Private'
						}

						var imgSub = v.image_name;
						if(imgSub.substr(0,3) == 'htt'){
							var img = v.image_name;
						}else{
							var img = imageUrl+'/upload/'+v.image_name;
						}

						html_c_podcasts += '<tr>\
												<td><img src="'+img+'" class="img-responsive" width="60" /></td>\
												<td>\
												<span class="text-normal text-xm text-primary-dark left">'+v.title+'</span>\
												</td>\
												<td><i class="fa fa-eye text-primary-dark"></i> '+status+'</td>\
												<td>'+v.created_date+'</td>\
												<td>'+v.count_play+'</td>\
											</tr>'
				});

				html_c_podcasts += '</tbody>\
								</table>\
								<div>'

				document.getElementById("viewContent").innerHTML = html_c_podcasts;

            }
            }
        });
        
    }