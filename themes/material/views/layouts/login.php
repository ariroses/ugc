<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Radio Aggregator - Login</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="Aggregator">
		<meta name="description" content="Radio Roov Aggregator">
		<!-- END META -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/bootstrap.css?1422792965" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/materialadmin.css?1425466319" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/font-awesome.min.css?1422529194" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/material-design-iconic-font.min.css?1421434286" />
		<!-- END STYLESHEETS -->
		<style>
			body {
				background: linear-gradient(to bottom right, #000, #5b5f5c);
				color: #fff;
			}
			.form-control {
    			border-bottom-color: rgba(126, 126, 131, 0.9);
			}
			.cstm-form {
				padding: 20px;
				background: #0000001a;
				border: 1px solid #0000001a;
			}
			.form-control {
				color: #fff;
			}
			.errorMessage{
				color: brown;
			}
		</style>
	</head>

	<body class="menubar-hoverable header-fixed ">

		<!-- BEGIN LOGIN SECTION -->
		<section class="section-account">
			<div class="card contain-xs style-transparent">
				<div class="card-body">
					<div class="row">
					<br/><br/><br/><br/><br/>
						<!-- <center> -->
						<?= $content; ?>
						<!-- </center> -->
					</div><!--end .row -->
				</div><!--end .card-body -->
			</div><!--end .card -->
		</section>
		<!-- END LOGIN SECTION -->

		<!-- BEGIN JAVASCRIPT -->
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery/jquery-1.11.2.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/spin.js/spin.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/App.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppNavigation.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppOffcanvas.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppCard.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppForm.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppNavSearch.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppVendor.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/demo/Demo.js"></script>
		<!-- END JAVASCRIPT -->

	</body>
</html>
