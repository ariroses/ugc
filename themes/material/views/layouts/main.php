<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Roov</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="Roov">
		<meta name="description" content="Roov">
		<!-- END META -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/bootstrap.css?1422792965" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/materialadmin.css?1425466319" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/font-awesome.min.css?1422529194" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/rickshaw/rickshaw.css?1422792967" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/morris/morris.core.css?1420463396" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/material-design-iconic-font.min.css?1421434286" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/select2/select2.css?1424887856" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/multi-select/multi-select.css?1424887857" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/bootstrap-tagsinput/bootstrap-tagsinput.css?1424887862" />
		<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858" />
		<!-- END STYLESHEETS -->
		<!-- toastr -->
  		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/theme-default/libs/toastr/toastr.css" />

		<!-- DATATABLES -->
		<script type="text/javascript">
		  var assetsPath = "<?php echo Yii::app()->baseUrl; ?>";
		  var imageUrl = "<?php echo Yii::app()->params['image_url']; ?>"
		</script>

		<style>
			#menubar.menubar-inverse::before {
			    background: linear-gradient(to bottom right, #000, #0b4848);
			}
			.sow{
				display: block;
			}
			.hid{
				display: none;
			}
			.c-img{
				width:120px; height: 120px;
			}
			.c-images{
				width:50px; height: 50px;
			}
		</style>
	</head>
	<body class="menubar-hoverable header-fixed menubar-pin">
		<!-- BEGIN HEADER-->
		<header id="header" >
			<div class="headerbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="headerbar-left">
					<ul class="header-nav header-nav-options">
						<li class="header-nav-brand" >
							<div class="brand-holder">
								<a href="<?= Yii::app()->getBaseUrl() ?>">
									<img class="img img-responsive" src="<?= Yii::app()->theme->baseUrl ?>/img/roov.png" width="100">
								</a>
							</div>
						</li>
						<li>
							<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
								<i class="fa fa-bars cstm-color"></i>
							</a>
						</li>
					</ul>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="headerbar-right">
					<ul class="header-nav header-nav-options">
						<li>
							<!-- Search form -->
							<form class="navbar-search" role="search">
								<div class="form-group">
									<input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
								</div>
								<button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
							</form>
						</li>
					</ul>

					<ul class="header-nav header-nav-profile">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
								<img src="<?php echo Yii::app()->session['avatar']; ?>" alt="" />
							</a>
							<ul class="dropdown-menu animation-dock">
								<li><a href="<?= Yii::app()->createUrl('') ?>">About</a></li>
								<li><a href="<?= Yii::app()->createUrl('/site/logout'); ?>"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
							</ul><!--end .dropdown-menu -->
						</li><!--end .dropdown -->

					</ul><!--end .header-nav-profile -->

				</div><!--end #header-navbar-collapse -->
			</div>
		</header>
		<!-- END HEADER-->

		<!-- BEGIN BASE-->
		<div id="base">

			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->

			<!-- BEGIN CONTENT-->
			<!-- <div id="content"> -->
				<section>
					<?= $content; ?>
				</section>
			<!-- </div>end #content -->
			<!-- END CONTENT -->

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="">
				<br>
				<div class="col-md-12">
					<img id="img_ava" class="img-circle border-gray border-xl img-responsive c-img" src="<?php echo Yii::app()->session['avatar']; ?>" alt="" style="margin:0px auto;" />
					<center id="name_sess" class="">
						<h4>Akun Anda</h4>
						<span class="profile-info cstm-color">
							<?php echo Yii::app()->session['name']; ?>
						</span>
					</center>
				</div>

				<div class="menubar-scroll-panel">

					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">

						<!-- BEGIN DASHBOARD -->
						<li id="Dasbor" class="" >
							<a style="cursor:pointer">
								<div class="gui-icon"><i class="fa fa-th-large"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END DASHBOARD -->

						<!-- BEGIN UI -->
						<li id="Podcast" class="">
							<a style="cursor:pointer">
								<div class="gui-icon"><i class="fa fa-podcast"></i></div>
								<span class="title">Podcast</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END UI -->

						<!-- BEGIN UI -->
						<!-- <li>
							<a style="cursor:pointer">
								<div class="gui-icon"><i class="fa fa-area-chart"></i></div>
								<span class="title">Analytic</span>
							</a>
						</li>--><!--end /menu-li -->
						<!-- END UI -->

						<!-- BEGIN UI -->
						<li id="Komen" class="">
							<a style="cursor:pointer">
								<div class="gui-icon"><i class="fa fa-comments"></i></div>
								<span class="title">Komentar</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END UI -->

						<!-- BEGIN UI -->
						<!-- <li>
							<a style="cursor:pointer">
								<div class="gui-icon"><i class="fa fa-dollar"></i></div>
								<span class="title">Monetisasi</span>
							</a>
						</li>--><!--end /menu-li -->
						<!-- END UI -->

					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; 2020</span> <strong>Roov</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->

		</div><!--end #base-->
		<!-- END BASE -->

		<?php Yii::app()->clientscript->registerCoreScript('jquery');?>
		<!-- BEGIN JAVASCRIPT -->
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/spin.js/spin.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/select2/select2.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/multi-select/jquery.multi-select.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/inputmask/jquery.inputmask.bundle.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/moment/moment.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/flot/jquery.flot.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/flot/jquery.flot.time.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/flot/jquery.flot.resize.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/flot/jquery.flot.orderBars.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/flot/jquery.flot.pie.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/flot/curvedLines.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery-knob/jquery.knob.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/sparkline/jquery.sparkline.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/d3/d3.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/d3/d3.v3.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/raphael/raphael-min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/morris.js/morris.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/rickshaw/rickshaw.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/App.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppNavigation.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppOffcanvas.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppCard.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppForm.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppNavSearch.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/source/AppVendor.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/demo/Demo.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/demo/DemoFormComponents.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/core/demo/DemoPageTimeline.js"></script>
		<!-- toastr -->
  		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/toastr/toastr.js" ></script>
		<!-- END JAVASCRIPT -->

		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/ugc_dasbor.js" ></script>
	</body>
</html>
